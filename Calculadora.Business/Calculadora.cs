using System.Collections.Generic;

namespace Calculadora.Business
{
    public class Calculadora {

        private readonly Stack<int> _valores = new Stack<int>();

        public void Informar(int p0) {
            _valores.Push(p0);
        }

        public void Add() {
            var direito = _valores.Pop();
            var esquerdo = _valores.Pop();

            _valores.Push(direito + esquerdo);
        }

        public int Result {
            get { return _valores.Peek(); }
        }
    }
}