﻿#language:pt-br

Funcionalidade: ProcurarFraseEmBuscador
	Eu gostaria de encontrar em buscadores informações sobre o metodo GoHorse

Cenario: ProcurarPorGoHorseNoGoogle
Dado Eu abri o navegador
E informe "http://www.google.com.br"
E procure por "GoHorse" 
Então Espero que o primeiro resultado seja "www.gohorseprocess.com.br/"
Então Espero ver a pagina principal com titulo "GoHorse"

Cenario: AbrirPaginaDeBuscaBing
Dado Eu abri o navegador
E informe "http://www.bing.com.br"
Então Espero ver a pagina principal com titulo "Bing"
