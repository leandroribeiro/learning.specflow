﻿using System;
using System.Runtime.InteropServices;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace NavegandoNaInternet.Tests
{
    [Binding]
    public class ProcurarFraseEmBuscadorSteps
    {
        private RemoteWebDriver _driver;

        [Given(@"Eu abri o navegador")]
        public void DadoEuAbriONavegador()
        {
            var binary = new FirefoxBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
            var profile = new FirefoxProfile();

            _driver = new FirefoxDriver(binary, profile);

            Assert.IsNotNull(_driver);
        }

        [Given(@"informe ""(.*)""")]
        public void DadoInforme(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        [Given(@"procure por ""(.*)""")]
        public void DadoProcurePor(string p0)
        {
            // ------ Google Search Only ! ---------

            //http://www.seleniumhq.org/docs/03_webdriver.jsp

            // Find the text input element by its name
            var query = _driver.FindElement(By.Name("q"));

            // Input the search text
            query.SendKeys("GoHorse");

            // Now submit the form
            //query.Submit();
            query.SendKeys(Keys.Enter);

            // Google's search is rendered dynamically with JavaScript.
            // Wait for the page to load, timeout after 5 seconds

            //var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            //var title = wait.Until<IWebElement>((d) => d.FindElement(By.Id("resultStats")));

            // Google's search is rendered dynamically with JavaScript.
            // Wait for the page to load, timeout after 10 seconds
            var z = (new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until((x) => x.Title.ToLower().StartsWith("gohorse - pesquisa google")));

        }

        [Then(@"Espero que o primeiro resultado seja ""(.*)""")]
        public void EntaoEsperoQueOPrimeiroResultadoSeja(string p0)
        {
            // ------ Google Search Only ! ---------

            var query =
                @"#rso > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > cite:nth-child(1)";

            var element2 = _driver.FindElementByCssSelector(query);

            Assert.AreEqual(p0, element2.Text);
        }

        [Then(@"Espero ver a pagina principal com titulo ""(.*)""")]
        public void EntaoEsperoVerAPaginaPrincipalComTitulo(string titulo)
        {
            Assert.IsTrue(_driver.Title.Contains(titulo));
        }

        [AfterScenario()]
        public void CleanUp()
        {
            _driver.Close();
            _driver.Dispose();
        }
    }
}
