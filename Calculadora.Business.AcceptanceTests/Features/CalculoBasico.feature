﻿#language:pt-br

Funcionalidade: Adição
    Para evitar enganos
    Enquanto alguém com dificuldades em matemática
    Eu gostaria de facilitar a soma de dois números

Cenário: Adicionar dois números
    Dado que informei "50" para a calculadora
    E que informei "70" para a calculadora
    Quando eu pressionar "Add"
    Então o resultado deverá ser "120"

Cenário: Adicionar dois negativos
    Dado que informei "-27" para a calculadora
    E que informei "-48" para a calculadora
    Quando eu pressionar "Add"
    Então o resultado deverá ser "-75"