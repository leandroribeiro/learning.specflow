﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDD_Calculadora_Exemplo.Steps {

    [Binding]
    public class CalculoBasicoSteps {

        private readonly Calculadora.Business.Calculadora _calculadora = new Calculadora.Business.Calculadora();

        [Given(@"que informei ""(.*)"" para a calculadora")]
        public void DadoQueInformeiParaACalculadora(int p0) {
            _calculadora.Informar(p0);
        }

        [When(@"eu pressionar ""(.*)""")]
        public void QuandoEuPressionarAdd(string p0) {
            _calculadora.Add();
        }

        [Then(@"o resultado deverá ser ""(.*)""")]
        public void EntaoOResultadoDeveraSer(int p0) {
            Assert.AreEqual(p0, _calculadora.Result);
        }
    }
}
